/*global Postsdemo, Backbone, JST*/

Postsdemo.Views = Postsdemo.Views || {};

(function () {
  'use strict';

  Postsdemo.Views.Post = Backbone.View.extend({

    template: JST['app/scripts/templates/post.ejs'],

    el: '#container',

    initialize: function () {
		this.listenTo(app.posts, 'reset', this.addAll);
    },

    render: function () {
		  this.$el.html("from model");
    },
	
	addOne: function (post) {
		this.$el.append(this.template({"post" : post}));
	},

	// Add all items in the **Todos** collection at once.
	addAll: function () {
		this.$el.html('');
		app.posts.each(this.addOne, this);
	}

  });

})();
