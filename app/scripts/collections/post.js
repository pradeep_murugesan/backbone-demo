/*global Postsdemo, Backbone*/

Postsdemo.Collections = Postsdemo.Collections || {};

(function () {
  'use strict';

  Postsdemo.Collections.Post = Backbone.Collection.extend({

    model: Postsdemo.Models.Post,
    url: 'http://jsonplaceholder.typicode.com/posts'
  });

})();
