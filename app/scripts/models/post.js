/*global Postsdemo, Backbone*/

Postsdemo.Models = Postsdemo.Models || {};

(function () {
  'use strict';

  Postsdemo.Models.Post = Backbone.Model.extend({


    initialize: function() {
    },

    defaults: {
		
    },

    validate: function(attrs, options) {
    },

    parse: function(response, options)  {
      return response;
    }
  });

})();
