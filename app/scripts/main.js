/*global postsdemo, $*/


window.postsdemo = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  init: function () {
    'use strict';
    console.log('Hello from Backbone!');
  }
};
Postsdemo = {};
$(document).ready(function () {
  'use strict';

	window.app ={};
	app.posts = new Postsdemo.Collections.Post();
	app.posts.fetch({reset: true});
	var appView = new Postsdemo.Views.Post();
	

});
